# csurf

Patched build of `surf`.

## Table of contents
  - [Requirements <a name="requirements"></a>](#requirements-)
  - [Installation <a name="installation"></a>](#installation-)
  - [Usage <a name="usage"></a>](#usage-)
  - [Contributing <a name="contributing"></a>](#contributing-)
  - [License <a name="license"></a>](#license-)

## Requirements <a name="requirements"></a>

In order to build `surf` you need `GTK+` and `Webkit/GTK+` header files. Also, to use the functionality of the url-bar you need `dmenu`.

## Installation <a name="installation"></a>

You need to run:

```bash
make clean install  
```

## Usage <a name="usage"></a>

To run `surf`:

```bash
surf [URI]
```

Also, to run surf on [tabbed](https://tools.suckless.org/tabbed/):

```bash
surf-open.sh [URI]
```

## Contributing <a name="contributing"></a>
PRs are welcome.

## License <a name="license"></a>
[MIT](https://raw.githubusercontent.com/santilococo/cslock/master/LICENSE)

